module.exports = function(grunt) {

    grunt.config.set('symlink', {
        options: {
            // Enable overwrite to delete symlinks before recreating them
            overwrite: false,
            // Enable force to overwrite symlinks outside the current working directory
            force: false
        },
        // The "build/target.txt" symlink will be created and linked to
        // "source/target.txt". It should appear like this in a file listing:
        // build/target.txt -> ../source/target.txt
        explicit: {
            src: 'assets',
            dest: '.tmp/public/assets'
        }
    });



    grunt.loadNpmTasks('grunt-contrib-symlink');

};
