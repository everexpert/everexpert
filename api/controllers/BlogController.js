/**
 * BlogController
 *
 * @description :: Server-side logic for managing Blogs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var fs = require('fs'),
    path = require('path');
module.exports = {
    // findBlogs:function (req,res) {
    //     Blog.find({}).sort('createdAt DESC').exec(function(err, blogs) {
    //         if (err) {
    //             return res.json(err.status, {err: err});
    //         }
    //         res.json(blogs);
    //     });
    // },

    blogCount : function (req,res) {
        Blog.count({}).exec(function (err,totalBlog) {
            if (err) {
                return res.json(err.status, {err: err});
            }

            // console.log('total order',totalOrder);
            res.json({totalBlogs:totalBlog})

        })
    },
    findBlogsByPage :function (req,res) {
        //console.log('req.body.pageNum',req.body.pageNum);
        if(!req.body.hasOwnProperty('pageNum'))
            req.body.pageNum = 1;
        // Order.find({}).limit(100).sort('createdAt DESC').exec(function(err, orders) {
        Blog.find({}).paginate({page: req.body.pageNum}).sort('createdAt DESC').exec(function(err, blogs) {
            if (err) {
                return res.json(err.status, {err: err});
            }
            // console.log(orders);
            res.json(blogs);
        });
    },
    folderList: function  (req, res) {
        fs.readdir("assets/images/", function (err, files) {
            if (err) {
                throw err;
            }
            var allImages=[];
            var allFiles = files.filter(function(file) {
                if(file.substr(-4) === '.png' || file.substr(-4) === '.gif' || file.substr(-4) === '.jpg' || file.substr(-4) === '.jpeg'){
                    return file;
                }
            });
            for(i=0;i<allFiles.length;i++){
                allImages[i]='/images/'+allFiles[i];
            }
            var allFolders = files.filter(function(file) {
                if(file.substr(0, 1) !== '.' && file.substr(-4) !== '.png' && file.substr(-4) !== '.gif' && file.substr(-4) !== '.jpg' && file.substr(-5) !== '.jpeg'){
                    return file;
                }
            });

            res.json({ folders:allFolders, images:allImages});

        });


        // var returnData=[];
        // var dirname='assets/images/home';
        // fs.readdir(dirname, 'utf8', function (err, data) {
        //
        //     if (err) throw err;
        //     for(var i=0;i<data.length;i++){
        //          returnData[i]='/images/home/'+data[i];
        //     }
        //     // console.log(returnData);
        //     res.json({ homeImg:returnData});
        // });

        // fs.readdir('.tmp/public/images/home', function (err, data) {
        //     if (err) throw err;
        //     console.log(data);
        // });
        //
        // var __dirname='.tmp/public/images/home';

        // fs.realpath(__dirname, function(err, path) {
        //     if (err) {
        //         console.log(err);
        //         return;
        //     }
        //     console.log('Path is : ' + path);
        // });
        // fs.readdir(__dirname, function(err, files) {
        //     if (err) return;
        //     files.forEach(function(f) {
        //         console.log('Files: ' + f);
        //     });
        // });

    },


    upload: function  (req, res) {
        var returnData=[];
        var dirname='assets/images/'+req.body.returndata;
        fs.readdir(dirname, 'utf8', function (err, data) {
            if (err) throw err;
            for(var i=0;i<data.length;i++){
                returnData[i]='/images/'+req.body.returndata+"/"+data[i];
            }
            res.json({ folderimages:returnData});
        });

    },


    onlyUploadImage: function  (req, res) {
        console.log("file upload coming");
        if(req.method === 'GET')
            return res.json({'status':'GET not allowed'});
        var dirName = sails.config.appPath+'/assets/images/'+req.body.epectedDirectory+"/";
        var uploadFile = req.file('file');
        uploadFile.upload({
            maxBytes: 2000000,
            dirname: dirName
        },function onUploadComplete (err, files) {
            // if(req.body.hasOwnProperty('customImage')){
            //     var imageresize = files[0].fd.replace(req.body.epectedDirectory+"/",req.body.epectedDirectory+'/');
            //     gm(files[0].fd)
            //         .resize(372, '')
            //         .noProfile()
            //         .write(imageresize, function (err) {
            //             if (err)
            //                 console.log('upload error',err);
            //         });
            // }else{
            //     var autoimage = files[0].fd.replace(req.body.epectedDirectory+"/",req.body.epectedDirectory+'/');
            //     gm(files[0].fd)
            //         .resize(1920, '')
            //         .noProfile()
            //         .write(autoimage, function (err) {
            //             if (err)
            //                 console.log('upload error',err);
            //         });
            // }
            var baseAbsolutePath = process.cwd();
            var imagePath = files[0].fd.replace(baseAbsolutePath,'');
            res.json({status:200,customImage:imagePath});
        });
    },

    onlyDeleteImage: function (req,res) {
        if(req.method === 'GET')
            return res.json({'status':'GET not allowed'});
        var filePath = sails.config.appPath+"/assets"+req.body.filePath;
        if(filePath){
            console.log(filePath,'filePath');
            if (fs.existsSync(filePath)) {
                fs.unlink(filePath,function(err){
                    if(err) return console.log(err);
                    console.log('filePath deleted successfully');
                    res.json('filePath deleted successfully');
                });
            }
        }
    },

    createSymlink : function(req, res){
        console.log("symlink created");
        //this code temporarily placed and need to be placed at config/bootstrap.js folder
        var postsSource = path.join(process.cwd(), 'assets')
            , postsDest = path.join(process.cwd(), '.tmp/public/assets');

        fs.symlink(postsSource, postsDest, function (err) {
            //cb(err);
            res.json({status:200,data:{action:"success"}});
        });
    }


};

