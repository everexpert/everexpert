var nodemailer = require('nodemailer');

module.exports = {
  authenticate: function(req, res) {
    var email = req.param('email');
    var password = req.param('password');

    if (!email || !password) {
      return res.json(401, {err: 'username and password required'});
    }

    User.findOneByEmail(email, function(err, user) {
      if (!user) {
        return res.json(401, {err: 'invalid username or password'});
      }

      User.validPassword(password, user, function(err, valid) {
        if (err) {
          return res.json(403, {err: 'forbidden'});
        }

        if (!valid) {
          return res.json(401, {err: 'invalid username or password'});
        } else {
          res.json({user: user, token: sailsTokenAuth.issueToken({sid: user.id})});
        }
      });
    })
  },

  register: function(req, res) {
    //TODO: Do some validation on the input
    if (req.body.password !== req.body.confirmPassword) {
      return res.json(401, {err: 'Password doesn\'t match'});
    }

    User.create({email: req.body.email, password: req.body.password, userRole: req.body.userRole}).exec(function(err, user) {
      if (err) {
        res.json(err.status, {err: err});
        return;
      }
      if (user) {
        res.json({user: user, token: sailsTokenAuth.issueToken({sid: user.id})});
      }
    });
  },

  recover:function (req, res) {
    User.findOne(req.body, function(err, user) {
      if (err) {
        return res.json(err.status, {err: err});
      }
      if(user){
        var validationCode = new Date().getTime();
        User.update({id:user.id},{recover_pass_validation_code:validationCode}).exec(function afterwards(err, updated){
          if (err) {
          }
          console.log(updated,'time stamp');
        });
        //now send this validationCode to email.
        var transporter = nodemailer.createTransport();
        var mailSubject = "Paragon Agro Ltd. (Recover Password)";
        var mailFrom = 'customercare@paragon.com.bd';
        var mailTo = user.email;
        var mailBody="To reset your password please "+
            '<a href="http://paragon.com.bd/expertpanel#!/resetpassword/' + validationCode+'/'+user.email +'" target="_blank">'+ 'click here' + '</a>';
        console.log(mailTo);
        console.log(mailFrom);
        console.log(mailBody,'########### MailBody #################');
        console.log(transporter.sendMail({
          from: mailFrom,
          to: mailTo,
          subject: mailSubject,
          html: mailBody
        }));
        //return value to front-end.
        res.json({userEmail:user.email, validationCode:validationCode, findStatus:"Email found"});
      }else{
        //res.json({findStatus:});
        return res.json(401, {err: "Email not found"});
      }

    })


  },

  updateRole:function (req,res) {
    console.log(req.body)
    User.findOne(req.body.id,function(err, user){
      // console.log(user);
      if (err) {
        return res.json(err.status, {err: err});
      }
      if(user){
        User.update( {id:user.id},  {userRole:req.body.userRole}).exec(function afterwards(err, updated){
          if (err) {
            return res.json(err.status, {err: err});
          }
          if(updated)
          // console.log(updated);
            return res.json(updated);
        });

      }else{
        return res.json(401, {err: "Update failed"});
      }
    });
  },

  userUpdate:function(req, res){
    //console.log('token sid',req.token.sid);
    //console.log('body id',req.body.id);

    User.update({id:req.body.id},req.body).exec(function(err, updatedUser) {
      if (err) {
        res.json(err.status, {err: err});
        return;
      }
      if (updatedUser) {
        res.json(updatedUser);
      }
    });
  },

  findUser:function (req,res) {

    // console.log(req.body,"called findUser api");

    User.find({}).exec(function(err, users) {
      if (err) {
        res.json(err.status, {err: err});
        return;
      }
      if (users.length) {
        // console.log(users);
        res.json(users);
      }else{
        res.json({});
      }
    });

  },

  checkVarification:function(req,res){
    console.log(req.body);
    if(req.body.hasOwnProperty('userEmail') && req.body.userEmail!='' && req.body.hasOwnProperty('validationCode') && req.body.validationCode!='' ){
      User.findOne({email:req.body.userEmail},function(err, user){
        if (err) {
          return res.json(err.status, {err: err});
        }
        if(user){
          res.json({ verificationStatus:"Verification success"});
        }else{
          return res.json(401, {err: "Varification failed, email and verification code not matched"});
        }
      })
    }
  },

  resetPassword:function (req,res) {

    console.log(req.body,'calling api');

    if(req.body.hasOwnProperty('email') && req.body.email!='' && req.body.hasOwnProperty('validationCode') && req.body.validationCode!='' ){
      var searchObj = {email:req.body.email,recover_pass_validation_code:req.body.validationCode};
      console.log(searchObj);

      User.findOne(searchObj,function(err, user){
        if (err) {
          return res.json(err.status, {err: err});
        }
        if(user){

          User.update( {id:user.id},  {password:req.body.password,confirmPassword:req.body.confirmPassword,userStatus:req.body.userStatus,recover_pass_validation_code:'no-more-exists'}).exec(function afterwards(err2, updated){

            if (err2) {
              return res.json(err2.status, {err: err2});
            }

            console.log('Updated user to have name ' + updated);
          });



        }else{
          return res.json(401, {err: "Varification failed, email and verification code not matched"});
        }
      })
    }

  }







};
