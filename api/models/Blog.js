/**
 * Blog.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    blogId:{
      type:'string'
    },
    blogTitle:{
      type:'string'
    },
    blogSlug:{
      type:'string'
    },
    image:{
      type:'string'
    },
    blogDiscription:{
      type:'string'
    },
    blogShortDiscription:{
      type:'string'
    },
    authorName:{
      type:'string'
    },
    post_category:{
      model:'blogcategory'
    }
  }
};

