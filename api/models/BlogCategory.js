/**
 * BlogCategory.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    category_title: {
      type: 'string'

    },
    category_slug: {
      type: 'string'

    },
    category_desc: {
      type: 'string'
    },
    category_status: {
      type: 'string'//published, unpublished, trashed
    },
    posts: {
      collection: 'blog',
      via: 'post_category'
    }

  }
};

