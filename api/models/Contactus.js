/**
 * Order.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    phone:{
      type:'number'
    },
    email: {
      type: 'string'
      // required: true
    },
    contactmessage: {
      type: 'string'
      // required: true
    }

  }
};

