angular.module('app')
  .factory('CurrentUser', function(LocalService) {
      var currentUser;
    return {
      user: function() {
        if (LocalService.get('auth_token')) {
          return currentUser=angular.fromJson(LocalService.get('auth_token')).data.user;
        } else {
          return currentUser={};
        }
      }
    };
  });
