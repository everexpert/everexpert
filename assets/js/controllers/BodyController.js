angular.module('app')
    .controller('BodyController', ['$scope','LocalService','CurrentUser','Auth','$state','$http','$timeout','$rootScope','$stateParams','toastr', function ($scope,LocalService,CurrentUser,Auth,$state,$http,$timeout, $rootScope, $stateParams, toastr) {

        console.log($stateParams);
        $scope.pageTitle = {mainSolgan: ":: IT Solutions Provider ever"};
        $scope.Products=[];

        $scope.allDistrict = [
            {
                "name": "Dhaka",
                "bn_name": "ঢাকা"
            },
            {
                "name": "Chittagong",
                "bn_name": "চট্টগ্রাম"
            },
            {
                "name": "Sylhet",
                "bn_name": "সিলেট"
            },
            {
                "name": "Faridpur",
                "bn_name": "ফরিদপুর"
            },
            {
                "name": "Gazipur",
                "bn_name": "গাজীপুর"
            },
            {
                "name": "Gopalganj",
                "bn_name": "গোপালগঞ্জ"
            },
            {
                "name": "Jamalpur",
                "bn_name": "জামালপুর"
            },
            {
                "name": "Kishoreganj",
                "bn_name": "কিশোরগঞ্জ"
            },
            {

                "name": "Madaripur",
                "bn_name": "মাদারীপুর"
            },
            {

                "name": "Manikganj",
                "bn_name": "মানিকগঞ্জ"
            },
            {

                "name": "Munshiganj",
                "bn_name": "মুন্সিগঞ্জ"
            },
            {

                "name": "Mymensingh",
                "bn_name": "ময়মনসিং"
            },
            {

                "name": "Narayanganj",
                "bn_name": "নারায়াণগঞ্জ"
            },
            {

                "name": "Narsingdi",
                "bn_name": "নরসিংদী"
            },
            {
                "name": "Netrokona",
                "bn_name": "নেত্রকোনা"
            },
            {
                "name": "Rajbari",
                "bn_name": "রাজবাড়ি"
            },
            {
                "name": "Shariatpur",
                "bn_name": "শরীয়তপুর"
            },
            {
                "name": "Sherpur",
                "bn_name": "শেরপুর"
            },
            {

                "name": "Tangail",
                "bn_name": "টাঙ্গাইল"
            },
            {
                "name": "Bogra",
                "bn_name": "বগুড়া"
            },
            {
                "name": "Joypurhat",
                "bn_name": "জয়পুরহাট"
            },
            {
                "name": "Naogaon",
                "bn_name": "নওগাঁ"
            },
            {
                "name": "Natore",
                "bn_name": "নাটোর"
            },
            {
                "name": "Nawabganj",
                "bn_name": "নবাবগঞ্জ"
            },
            {
                "name": "Pabna",
                "bn_name": "পাবনা"
            },
            {
                "name": "Rajshahi",
                "bn_name": "রাজশাহী"
            },
            {
                "name": "Sirajgonj",
                "bn_name": "সিরাজগঞ্জ"
            },
            {
                "name": "Dinajpur",
                "bn_name": "দিনাজপুর"
            },
            {
                "name": "Gaibandha",
                "bn_name": "গাইবান্ধা"
            },
            {
                "name": "Kurigram",
                "bn_name": "কুড়িগ্রাম"
            },
            {
                "name": "Lalmonirhat",
                "bn_name": "লালমনিরহাট"
            },
            {
                "name": "Nilphamari",
                "bn_name": "নীলফামারী"
            },
            {
                "name": "Panchagarh",
                "bn_name": "পঞ্চগড়"
            },
            {
                "name": "Rangpur",
                "bn_name": "রংপুর"
            },
            {
                "name": "Thakurgaon",
                "bn_name": "ঠাকুরগাঁও"
            },
            {
                "name": "Barguna",
                "bn_name": "বরগুনা"
            },
            {
                "name": "Barisal",
                "bn_name": "বরিশাল"
            },
            {
                "name": "Bhola",
                "bn_name": "ভোলা"
            },
            {
                "name": "Jhalokati",
                "bn_name": "ঝালকাঠি"
            },
            {
                "name": "Patuakhali",
                "bn_name": "পটুয়াখালী"
            },
            {
                "name": "Pirojpur",
                "bn_name": "পিরোজপুর"
            },
            {
                "name": "Bandarban",
                "bn_name": "বান্দরবান"
            },
            {
                "name": "Brahmanbaria",
                "bn_name": "ব্রাহ্মণবাড়িয়া"
            },
            {
                "name": "Chandpur",
                "bn_name": "চাঁদপুর"
            },
            {
                "name": "Chittagong",
                "bn_name": "চট্টগ্রাম"
            },
            {
                "name": "Comilla",
                "bn_name": "কুমিল্লা"
            },
            {

                "name": "Cox''s Bazar",
                "bn_name": "কক্স বাজার"
            },
            {
                "name": "Feni",
                "bn_name": "ফেনী"
            },
            {
                "name": "Khagrachari",
                "bn_name": "খাগড়াছড়ি"
            },
            {
                "name": "Lakshmipur",
                "bn_name": "লক্ষ্মীপুর"
            },
            {
                "name": "Noakhali",
                "bn_name": "নোয়াখালী"
            },
            {
                "name": "Rangamati",
                "bn_name": "রাঙ্গামাটি"
            },
            {
                "name": "Habiganj",
                "bn_name": "হবিগঞ্জ"
            },
            {
                "name": "Maulvibazar",
                "bn_name": "মৌলভীবাজার"
            },
            {
                "name": "Sunamganj",
                "bn_name": "সুনামগঞ্জ"
            },
            {
                "name": "Sylhet",
                "bn_name": "সিলেট"
            },
            {
                "name": "Bagerhat",
                "bn_name": "বাগেরহাট"
            },
            {
                "name": "Chuadanga",
                "bn_name": "চুয়াডাঙ্গা"
            },
            {
                "name": "Jessore",
                "bn_name": "যশোর"
            },
            {
                "name": "Jhenaidah",
                "bn_name": "ঝিনাইদহ"
            },
            {
                "name": "Khulna",
                "bn_name": "খুলনা"
            },
            {
                "name": "Kushtia",
                "bn_name": "কুষ্টিয়া"
            },
            {
                "name": "Magura",
                "bn_name": "মাগুরা"
            },
            {
                "name": "Meherpur",
                "bn_name": "মেহেরপুর"
            },
            {
                "name": "Narail",
                "bn_name": "নড়াইল"
            },
            {
                "name": "Satkhira",
                "bn_name": "সাতক্ষীরা"
            }
        ];

        $scope.auth = Auth;
        $scope.currentUser="";
        $scope.setLoggedinInfo = function(){
            $scope.currentUser = CurrentUser.user();
        };
        $scope.setLoggedinInfo();

        $scope.logout = function() {
            Auth.logout();
            $state.go('anon.home');
        };

        $scope.openModal = function (modalId,urlSlug) {
            // console.log(modalId,'modalIdmodalIdmodalIdmodalId')
            $scope.expectedItem = $scope.Products.find(function (item) {
                return item.id==modalId;
            });
            // console.log($scope.expectedItem);
            $scope.expectedItem.showModal=true;
            $http.get("productreview?product="+$scope.expectedItem.id).then(function(response){
                $scope.reviews=response.data;

                if(urlSlug == 'popular' ){
                    $state.go("anon.home.productdetail",{productTitle:$scope.expectedItem.productName,productId:$scope.expectedItem.id,urlSlug:urlSlug});
                }else{
                    $state.go("anon.product.category.productdetail",{productTitle:$scope.expectedItem.productName,productId:$scope.expectedItem.id,urlSlug:urlSlug});
                }
                // console.log("ddgfgdfg",response);
            },function(err){
                if(err)
                    console.log(err);
            });
        };

        $scope.clickOnModalContent=false;
        $scope.closeMe = function () {
            if($scope.clickOnModalContent){
                $scope.clickOnModalContent=false;
            }else{
                $scope.expectedItem.showModal=false;
                $scope.expectedItem={};
            }
        };

        $scope.modalClick = function () {
            $scope.clickOnModalContent=true;
        };

        $scope.addSubscription = function(email,successCB,errorCB){
            //console.log('email',email);
            $http.post('/subscription/create',{email:email}).then(function(response){
                //console.log(response);
                successCB(response);
            },function(err){
                if(err)
                    console.log(err);
                errorCB(err);
            });
        };

        $scope.goTop = function () {
            $("html, body").animate({ scrollTop: 0 }, 1000);
        };

        $(window).scroll(function () {
            if ($(this).scrollTop() > 900) {
                $('.goToTop').fadeIn();
            } else {
                $('.goToTop').fadeOut();
            }
        });


        $scope.review = {};
        $scope.productreviews = function (expectedItem) {
            if ($scope.review.reviewtext === undefined || $scope.review.reviewtext =='' || $scope.review.name === undefined || $scope.review.name =='' || $scope.review.email === undefined || $scope.review.email ==''){
                toastr.error('Please fill up the form', 'Error!');
                return;
            }else {
                if($scope.expectedItem.hasOwnProperty('id')){
                    $scope.review.product=expectedItem.id;
                    $scope.review.product_status="pending";
                    $http.post('/productreview',$scope.review).then(function(response){
                        console.log(response);
                        // console.log("id", id);
                        $scope.review = {};
                        toastr.success('Your Review posted successfully, wait for expert moderation');
                    },function(err){
                        if(err)
                            console.log(err);
                    });

                }else {
                    console.log('error');
                }

            }
            // console.log(expectedItem, 'expectedItem');
            //
            // console.log($scope.Products);
            // //

        } ;

    }]);


