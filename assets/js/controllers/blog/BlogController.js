angular.module('app')
    .controller('BlogController', ['$scope','$sce','$http','toastr', function ($scope, $sce,$http,toastr) {


        $scope.Blogs=[];


        $scope.blogCategories=[];

        $http.get('blogcategory').then(function (response) {
            // console.log(response);
            if(response.data.length){
                $scope.blogCategories = response.data;
            }
        },function (err) {
            if(err)
                console.log(err);
        })

        $scope.returnMessage='';
        $scope.subscribeMe = function(email){
            $scope.returnMessage = $scope.addSubscription(email,function(response){
                // console.log(response);
                $scope.returnMessage="Successfully Subscribed";
                // toastr.success('Success!', 'Successfully Subscribed');
            },function(err){
                console.log(err);
                toastr.error('error!', 'Subscription Failed, You May Already Subscribed With This Email');
            });

        };

        $scope.renderHtml = function(code)
        {
            //code = '<img src="'+$scope.post.post_image+ '" class="pull-right" alt=""/>'+code;
            return $sce.trustAsHtml(code);
        };
        $http.get('blog').then(
            function(blog){
                $scope.blog = blog.data;
            }
        );
        // $scope.orderProp = 'age';
        $scope.quantity = 8;



    }]);
