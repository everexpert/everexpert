angular.module('app')
    .controller('BlogCategoryController', ['$scope','$stateParams','$http', function ($scope,$stateParams,$http) {

        // console.log($stateParams);
        $scope.blogCategory='';
        if($stateParams.hasOwnProperty('categoryId') && $stateParams.categoryId!='all'){
            $http.get('blog/find?post_category='+$stateParams.categoryId).then(function (response) {
                // console.log(response);
                if(response.data.length)
                    $scope.Blogs = response.data;
            },function (err) {
                if(err)
                    console.log(err);
            })
        }else if($stateParams.hasOwnProperty('categoryId') && $stateParams.categoryId=='all'){
            $http.get('blog').then(function (response) {
                // console.log(response);
                if(response.data.length)
                    $scope.Blogs = response.data;
            },function (err) {
                if(err)
                    console.log(err);
            })
        }
            // $scope.blogCategory=$stateParams.categoryId;
            // console.log($scope.blogCategory);

    }]);