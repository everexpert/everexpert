angular.module('app')
    .controller('BlogDetailsController', ['$scope','$stateParams','$sce','$http','toastr', function ($scope,$stateParams,$sce,$http, toastr) {

        // console.log($stateParams);



        //$scope.khaasfood = $scope.Blogs.filter(function (item) {
        //    return item.category == "khaasfood";
        //});
        //$scope.khaasfoodLength = $scope.khaasfood.length;
        //
        //$scope.honey = $scope.Blogs.filter(function (item) {
        //    return item.category == "honey";
        //});
        //$scope.honeyLength = $scope.honey.length;
        //
        //$scope.dryfish = $scope.Blogs.filter(function (item) {
        //    return item.category == "dryfish";
        //});
        //$scope.dryfishLength = $scope.dryfish.length;
        //
        //$scope.grocery = $scope.Blogs.filter(function (item) {
        //    return item.category == "grocery";
        //});
        //$scope.groceryLength = $scope.grocery.length;
        //
        //$scope.organic = $scope.Blogs.filter(function (item) {
        //    return item.category == "organic";
        //});
        //$scope.organicLength = $scope.organic.length;
        //
        //$scope.wintercollection = $scope.Blogs.filter(function (item) {
        //    return item.category == "wintercollection";
        //});
        //$scope.wintercollectionLength = $scope.wintercollection.length;




        $scope.blog = {};
        $scope.comments = {};
        if($stateParams.hasOwnProperty("blogId")){
            $http.get("blog/"+$stateParams.blogId).then(function(response){
                $scope.blog=response.data;
                // console.log($scope.blog);
                $scope.pageTitle.othePageTitle = $scope.blog.blogSlug;
            },function(err){
                if(err)
                    console.log(err);
            });

            // $http.post('blogcomment/getBlogComment/',{blog:$stateParams.blogId,blog_status:"approved"}).then(function(response){
            $http.post('bloggcomment/find?blog='+$stateParams.blogId+'&comment_status=approved').then(function(response){
                if(response.data.length)
                    $scope.comments=response.data;
                // console.log($scope.comments);
            },function(err){
                if(err)
                    console.log(err);
            })
        }
        // if($stateParams.hasOwnProperty("blogTitle")){
        //     $http.get("blog/"+$stateParams.blogTitle).then(function(response){
        //         $scope.blog=response.data;
        //         console.log($scope.blog);
        //     },function(err){
        //         if(err)
        //             console.log(err);
        //     });
        //
        //     // $http.post('blogcomment/getBlogComment/',{blog:$stateParams.blogId,blog_status:"approved"}).then(function(response){
        //     $http.post('blogcomment/find?blog='+$stateParams.blogId+'&blog_status=approved').then(function(response){
        //         if(response.data.length)
        //             $scope.comments=response.data;
        //         console.log($scope.comments);
        //     },function(err){
        //         if(err)
        //             console.log(err);
        //     })
        // }




        $scope.comment = {};
        $scope.blogComments = function () {
            if ($scope.comment.name === undefined || $scope.comment.name =='' || $scope.comment.email === undefined || $scope.comment.email =='' || $scope.comment.comment_text === undefined || $scope.comment.comment_text ==''){
                toastr.error('Please fill up the form', 'Error!');
                return;
            }else {
                if($scope.blog.hasOwnProperty("id")){
                    $scope.comment.blog=$scope.blog.id;
                    $scope.comment.comment_status="pending";
                    $http.post('/bloggcomment',$scope.comment).then(function(response){
                        console.log(response);
                        $scope.comment = {};
                        toastr.success('Your Comment posted successfully, wait for expert moderation');
                    },function(err){
                        if(err)
                            console.log(err);
                    });

                }else {
                    console.log('error');
                }

            }
            };





        $scope.backtotop = function () {
            $( "html body" ).scrollTop( 0 );
        };
        $scope.renderHtml = function(code) {
            //code = '<img src="'+$scope.post.post_image+ '" class="pull-right" alt=""/>'+code;
            return $sce.trustAsHtml(code);
        };



    }]);
