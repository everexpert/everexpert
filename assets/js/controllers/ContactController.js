angular.module('app')
    .controller('ContactController', ['$scope','$http','$state','toastr', function ($scope, $http, $state, toastr) {
        $scope.mailer={};
        $scope.sendMail = function () {
            console.log('$scope.mailer.name',$scope.mailer.name);
            if ($scope.mailer.name === undefined || $scope.mailer.name =='' || $scope.mailer.email === undefined || $scope.mailer.email =='' || $scope.mailer.contactmessage === undefined || $scope.mailer.contactmessage ==''){
                toastr.error('Please fill up the form', 'Error!');
                return;
            }else {
                console.log($scope.mailer);
                $http.post('/contactus/contactMail', $scope.mailer).then(function successCallback(response) {
                    toastr.success('Success!', 'Sent Message');

                    console.log("success",response.data);

                    $state.go("anon.contact");

                }, function errorCallback(response) {
                    console.log("error",response)
                });
                $scope.mailer={};
            }
        };

        var headOffice = new L.LayerGroup();
        var chittagong = new L.LayerGroup();
        var dhanmondiOt = new L.LayerGroup();
        var shylet = new L.LayerGroup();
        var tejgonot = new L.LayerGroup();
        var gulsanOt = new L.LayerGroup();

        console.log(dhanmondiOt);
        L.marker([23.762861, 90.364267], {icon: L.AwesomeMarkers.icon({icon: 'home',  markerColor: 'green', prefix: 'fa'}) }).bindPopup('<img class="img-responsive" src="/images/home/ee-logo-vertical.png" alt="" style="width: 100%"><br><hr><p class="text-center">26/28/29, Sher Shah-Shwari Road<br>Mohammadpur, Dhaka-1207</p>').addTo(headOffice);
        L.marker([22.3523, 91.8123], {icon: L.AwesomeMarkers.icon({icon: 'shopping-cart',  markerColor: 'darkpuple', prefix: 'fa'}) }).bindPopup('Khaasfood, Chittagong').addTo(chittagong);
        L.marker([24.899452, 91.868698], {icon: L.AwesomeMarkers.icon({icon: 'shopping-cart',  markerColor: 'cadetblue', prefix: 'fa'}) }).bindPopup('Khaasfood, Shylet').addTo(shylet);

        var mbAttr = '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="http://mapbox.com">Mapbox</a>',
            mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoic2hpaGFiIiwiYSI6ImNpemM5Z2k2MDAwM3Qyd254cjQ4aWt2YWUifQ.eaKhSLr_0WkWUkEq5hhPpg';


        var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr, mapId: 'shihab.21nd6o54', token: 'pk.eyJ1Ijoic2hpaGFiIiwiYSI6ImNpemM5Z2k2MDAwM3Qyd254cjQ4aWt2YWUifQ.eaKhSLr_0WkWUkEq5hhPpg'}),
            streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr, mapId: 'shihab.21nd6o54', token: 'pk.eyJ1Ijoic2hpaGFiIiwiYSI6ImNpemM5Z2k2MDAwM3Qyd254cjQ4aWt2YWUifQ.eaKhSLr_0WkWUkEq5hhPpg'});
        var map = L.map('map', {
            center: [23.762861, 90.364267],
            zoom: 17,
            layers: [streets, headOffice],
            scrollWheelZoom: false
        });
        // map.setView(markersLayer.getBounds().getCenter());
        var baseLayers = {
            "Grayscale": grayscale,
            "Streets": streets
        };
        var overlays = {
            "Chittagong": chittagong,
            "Shylet": shylet,
            "Head Office": headOffice
        };
        L.control.layers(baseLayers, overlays).addTo(map);

        $("#office").click(function(event) {
            map.setView([23.762861, 90.364267], 17);
            event.preventDefault();
            if(map.hasLayer(headOffice)) {
                $(this).removeClass('selected');
                // map.removeLayer(headOffice);
            } else {
                // map.addLayer(headOffice);
                $(this).addClass('selected');
            }
        });

        $("#chittagong").click(function(event,mapshow) {

            map.setView([22.3523, 91.8123], 14);


            event.preventDefault();
            if(map.hasLayer(chittagong)) {
                $(this).removeClass('selected');
                // map.removeLayer(mohammadpurOt);
            } else {
                map.addLayer(chittagong);
                // $(this).addClass('selected');
            }
        });

        $("#shylet").click(function(event) {
            map.setView([24.899452, 91.868698], 14);

            event.preventDefault();
            if(map.hasLayer(shylet)) {
                $(this).removeClass('selected');
                // map.removeLayer(mohakhaliot);
            } else {
                map.addLayer(shylet);
                // $(this).addClass('selected');
            }
        });
        $scope.returnMessage='';
        $scope.subscribeMe = function(email){
            $scope.returnMessage = $scope.addSubscription(email,function(response){
                console.log(response);
                // toastr.success('Success!', 'Successfully Subscribed');
                $scope.returnMessage="Successfully Subscribed";
            },function(err){
                console.log(err);
                toastr.error('Subscription Failed, You May Already Subscribed With This Email', 'Error!');
                // $scope.returnMessage="Subscription Failed, You May Already Subscribed With This Email";
            });
            console.log('$scope.returnMessage',$scope.returnMessage);
        }
    }]);
