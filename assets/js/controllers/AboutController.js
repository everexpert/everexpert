angular.module('app')
    .controller('AboutController', ['$scope', function ($scope) {

        $scope.clients=[
            {
                "image" : "images/About us/khaasfood.jpg"
            },
            {
                "image" : "images/About us/logo70.png"
            },
            {
                "image" : "images/About us/logo-SB.jpg"
            },
            {
                "image" : "images/About us/brideg-holding-logo.png"
            },
            {
                "image" : "images/About us/logocfg.png"
            },
            {
                "image" : "images/About us/sass-logo.png"
            },
            {
                "image" : "images/About us/javacup.png"
            },
            {
                "image" : "images/About us/logo-pergon-llll.png"
            }
        ];
        $scope.technologies=[
            {
                "image" :"images/About us/angular.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/bootstrap.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/css3.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/html.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/jquery.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/React_logo_wordmark.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/wordpress.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/leaflet.jpg",
                category: 'frontend'
            },
            {
                "image" :"images/About us/nodejs.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/loopback.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/sails.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/django.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/python.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/laravel.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/symfony_logo.jpg",
                category: 'backend'
            },
            {
                "image" :"images/About us/mysql.jpg",
                category: 'database'
            },
            {
                "image" :"images/About us/oracle.jpg",
                category: 'database'
            },
            {
                "image" :"images/About us/mongo.jpg",
                category: 'database'
            },
             {
                "image" :"images/About us/neo4j_logo.jpg",
                category: 'database'
            },
             {
                "image" :"images/About us/postgresql-logo.jpg",
                category: 'database'
            },
            {
                "image" :"images/About us/android.jpg",
                category: 'mobileapps'
            },
            {
                "image" :"images/About us/ionic-logo.jpg",
                category: 'mobileapps'
            },
            {
                "image" :"images/About us/react-native.png",
                category: 'mobileapps'
            },
            {
                "image" :"images/About us/apache.jpg",
                category: 'server'
            },
            {
                "image" :"images/About us/nodeserver-logo.jpg",
                category: 'server'
            },
            {
                "image" :"images/About us/NGINX_logo_rgb-01.jpg",
                category: 'server'
            },
            {
                "image" :"images/About us/googleAnalytics.jpg",
                category: 'analytics'
            },
            {
                "image" :"images/About us/Gephi-logo.jpg",
                category: 'analytics'
            },
            {
                "image" :"images/About us/Logo_D3.jpg",
                category: 'analytics'
            },
            {
                "image" :"images/About us/Git-Logo-2Color.jpg",
                category: 'scm'
            },
            {
                "image" :"images/About us/Atlassian_Bitbucket_Logo.jpg",
                category: 'scm'
            }

        ];

        $scope.backtotop = function () {
            $( "html body" ).scrollTop( 0 );
        };
        // $(window).scroll(function () {
        //     if ($(this).scrollTop() > 400) {
        //         $('.goToTop').fadeIn();
        //     } else {
        //         $('.goToTop').fadeOut();
        //     }
        // });
        // $('.goToTop').click(function () {
        //     $("html, body").animate({ scrollTop: 0 }, 1000);
        //     // return false;
        // });

        // if(!$scope.addToCartHidden){
        //     $scope.addToCartHidden=true;
        // }else {
        //     $scope.addToCartHidden=true;
        // }
        //
        //
        // $scope.$emit('shouldAddToCartGlobal', $scope.addToCartHidden);



    }]);