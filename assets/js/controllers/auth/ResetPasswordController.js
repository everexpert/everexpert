angular.module('app')
    .controller('ResetPasswordController', function($scope, $state, Auth, $stateParams, $timeout, toastr) {

        console.log($stateParams);
        $scope.errors = [];
        $scope.errors2 = [];
        $scope.user={};

        if($stateParams.hasOwnProperty("userEmail") && $stateParams.userEmail!="" && $stateParams.hasOwnProperty("validationCode")  && $stateParams.validationCode!=""){
            Auth.checkVarification($stateParams).then(function(response){
                    console.log(response);
                },function(err){
                    if(err)
                        console.log(err);
                $scope.errors.push(err.data.err);

            });
            console.log("ekhane aise");
        }

        $scope.resetPassword = function(){
            console.log("password rest korte aise");
            if(!$scope.user.hasOwnProperty("password") || $scope.user.password==''){
                $scope.errors2 = [];
                $scope.errors2.push("Empty password");
                return;
            }
            if(!$scope.user.hasOwnProperty("confirmPassword") || $scope.user.confirmPassword==''){
                $scope.errors2 = [];
                $scope.errors2.push("Empty confirmPassword");
                return;
            }
            if($scope.user.password != $scope.user.confirmPassword){
                $scope.errors2 = [];
                $scope.errors2.push("Password not matched");
                return;
            }

            $scope.errors2 = [];
            $scope.user.userStatus = "changed";
            $scope.user.email = $stateParams.userEmail;
            $scope.user.validationCode = $stateParams.validationCode;
            toastr.success('You have successfully reset your password');
            Auth.resetPassword($scope.user).then(function(response){
                console.log(response);
            },function(err){
                if(err)
                    console.log(err);
            });

            $timeout(function () {
                $state.go('anon.login');
            }, 3000);

        }

    });
