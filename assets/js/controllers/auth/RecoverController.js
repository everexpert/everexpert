angular.module('app')
    .controller('RecoverController', function($scope, $state, Auth) {

        $scope.user={};

        $scope.foundEmail=false;
        $scope.send=function () {
            console.log($scope.user);
            Auth.recover($scope.user)
                .then(function(result) {
                    console.log("result",result);
                    if(result.data.findStatus=="Email found")
                        $scope.foundEmail ="Please check your email.";

                },function(err) {
                    console.log(err);
                });

        }

    });
