angular.module('app')
  .controller('RegisterController', function($scope, $state, Auth) {
      $scope.errors = [];
      $scope.user={};
      $scope.register = function() {
          $scope.errors = [];
          if(!$scope.user.hasOwnProperty("email") || $scope.user.email==''){
            $scope.errors = [];
            $scope.errors.push("Empty email");
            return;
          }

          if(!$scope.user.hasOwnProperty("password") || $scope.user.password==''){
            $scope.errors = [];
            $scope.errors.push("Empty password");
            return;
          }

          if(!$scope.user.hasOwnProperty("confirmPassword") || $scope.user.confirmPassword==''){
            $scope.errors = [];
            $scope.errors.push("Empty confirmPassword");
            return;
          }

          if($scope.user.password != $scope.user.confirmPassword){
            $scope.errors = [];
            $scope.errors.push("Password not matched");
            return;
          }

          $scope.errors = [];

          Auth.register($scope.user).then(function(user) {
            console.log(user);
            //$scope.currentUser=result.data.user;
            $scope.setLoggedinInfo();
            $state.go('anon.home');
          },function(err) {
            console.log(err);

            if(err.data.err.hasOwnProperty("invalidAttributes") && err.data.err.invalidAttributes.hasOwnProperty("email"))
              $scope.errors.push(err.data.err.invalidAttributes.email[0].message);

            if(err.data.err=="Password doesn't match"){
              console.log("ekhane aise");
              $scope.errors.push(err.data.err);
            }
            //$scope.errors.push(err.data);
            console.log(err.data);
          });

    }

  });
