angular.module('app')
  .controller('LoginController', function($scope, $state, Auth,$http) {
    $scope.errors = [];
      $scope.user={};

        $scope.login = function() {
            $scope.errors = [];

            $http.post("/auth/findMe",{email:$scope.user.email}).then(function (response) {
                console.log(response);
                if(response.data.userStatus=='oldUser'){
                    $state.go("anon.requestToChangePassword",{email:response.data.email});
                }else if(response.data.userStatus=='notOldUser'){
                    Auth.login($scope.user)
                        .then(function(result) {
                            console.log("result",result);
                            $scope.setLoggedinInfo();
                            $state.go('annon.home');
                        },function(err) {
                            console.log(err);
                            $scope.errors.push(err.data.err);
                        });
                }else{
                    $scope.errors.push(response.data.userStatus + " Please enter valid email address");
                }
            },function (err) {
                if(err)
                    console.log(err);
            });


            // Auth.login($scope.user)
            //     .then(function(result) {
            //         console.log("result",result);
            //         $scope.setLoggedinInfo();
            //
            //         if(result.data.user.userStatus=="old"){
            //             $state.go('user.myaccount.olduser',{result:result});
            //         }else{
            //             $state.go('user.myaccount.order');
            //         }
            //
            //     },function(err) {
            //         console.log(err);
            //         $scope.errors.push(err.data.err);
            //     });
        };




      // $scope.onKeyPress = function ($event) {
      //     console.log($event);
      // };


  });
