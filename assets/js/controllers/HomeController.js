angular.module('app')
    .controller('HomeController', ['$scope','$timeout','$rootScope','$sce', function ($scope,$timeout, $rootScope, $sce) {
        angular.element(document).ready(function (){
            $(window).scroll(function() {

                if ($(window).scrollTop() > 100) {
                    $('.main_h').addClass('sticky');
                } else {
                    $('.main_h').removeClass('sticky');
                }
            });


            $(".regular.howtoOrderslider").slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1

            });

        });

        $('.slider1').slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 2000,
            mobileFirst: true
        });
        $scope.IsVisible = false;
        $scope.ShowHide = function () {
            $scope.IsVisible = $scope.IsVisible ? false : true;
        };
        $scope.renderHtml = function(code) {
            //code = '<img src="'+$scope.post.post_image+ '" class="pull-right" alt=""/>'+code;
            return $sce.trustAsHtml(code);
        };
    }])
    .directive('popularProductDirective',function($timeout){
    return {
        restrict: 'A',
        link: function(scope,element,attrs) {
            $timeout(function() {
                $(".regular.slider").not('.slick-initialized').slick({
                    dots: true,
                    autoplay: true,
                    infinite: true,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: "unslick"
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }]
                });
            });
        }
    }
});