angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider, AccessLevels) {

        $stateProvider
            .state('anon', {
                abstract: true,
                template: '<ui-view/>',
                data: {
                    access: AccessLevels.anon
                }
            })
            .state('anon.home', {
                url: '/',
                templateUrl: 'home.html',
                controller: 'HomeController'
            })
            .state('anon.about', {
                url: '/about',
                templateUrl: 'templates/about.html',
                controller: 'AboutController'
            })
            .state('anon.about.all', {
                url: '/all',
                templateUrl: 'templates/about/alltools.html',
                controller: 'FrontendDevelopmentController'
            })
            .state('anon.about.frontend_development', {
                url: '/ui-design',
                templateUrl: 'templates/about/frontendDevelopment.html',
                controller: 'FrontendDevelopmentController'
            })
            .state('anon.about.backend_development', {
                url: '/web-application',
                templateUrl: 'templates/about/backendDevelopment.html',
                controller: 'BackendDevelopmentController'
            })
            .state('anon.about.mobile_application', {
                url: '/mobile-application',
                templateUrl: 'templates/about/mobileApplication.html',
                controller: 'BackendDevelopmentController'
            })
            .state('anon.about.database', {
                url: '/database',
                templateUrl: 'templates/about/database.html',
                controller: 'BackendDevelopmentController'
            })
            .state('anon.about.server', {
                url: '/server',
                templateUrl: 'templates/about/server.html',
                controller: 'BackendDevelopmentController'
            })
            .state('anon.about.analytics', {
                url: '/analytics',
                templateUrl: 'templates/about/analytics.html',
                controller: 'BackendDevelopmentController'
            })
            .state('anon.about.scm', {
                url: '/source-code-management',
                templateUrl: 'templates/about/scm.html',
                controller: 'BackendDevelopmentController'
            })
            .state('anon.websolution', {
                url: '/websolution',
                templateUrl: 'templates/service/websotion.html',
                controller: 'webSolutionController'
            })
            .state('anon.softwaresolution', {
                url: '/softwaresolution',
                templateUrl: 'templates/service/softwaresolution.html',
                controller: 'softwareSolutionController'
            })
            .state('anon.digitalmarketing', {
                url: '/digitalmarketing',
                templateUrl: 'templates/service/digitalmarketing.html',
                controller: 'digitalMarketingController'
            })
            .state('anon.corporatebranding', {
                url: '/corporatebranding',
                templateUrl: 'templates/service/corporatebranding.html',
                controller: 'corporateBrandingController'
            })

            .state('anon.blog', {
                abstract: true,
                url: '/portfolio',
                templateUrl: 'templates/portfolio/portfolio.html',
                controller: 'BlogController'
            })
            .state('anon.blog.category', {
                url: '/category/:categorySlug/:categoryId',
                templateUrl: 'templates/portfolio/portfolio.category.html',
                controller: 'BlogCategoryController'
            })
            .state('anon.blog.details', {
                url: '/portfolio-details/:categorySlug/:blogSlug/:blogId',
                templateUrl: 'templates/portfolio/portfoliodetails.html',
                controller: 'BlogDetailsController'
            })

            .state('anon.contact', {
                url: '/contact',
                templateUrl: 'templates/contact.html',
                controller: 'ContactController'
            })
            .state('anon.login', {
                url: '/login',
                templateUrl: 'auth/login.html',
                controller: 'LoginController'
            })
            .state('anon.register', {
                url: '/register',
                templateUrl: 'auth/register.html',
                controller: 'RegisterController'
            })
            .state('anon.recover', {
                url: '/recover',
                templateUrl: 'auth/recover.html',
                controller: 'RecoverController'
            })
            .state('anon.resetpassword', {
                url: '/resetpassword/:validationCode/:userEmail',
                templateUrl: 'auth/resetPassword.html',
                controller: 'ResetPasswordController'
            })
            .state('anon.requestToChangePassword', {
                url: '/request-to-change-password/:email',
                templateUrl: 'auth/olduser.html',
                controller: 'oldUserController'
            })


        $urlRouterProvider.otherwise('/');

    });