angular.module('app', ['ui.bootstrap', 'ui.router', 'ngMessages', 'app-templates', 'toastr'])
    .config(["$locationProvider", function($locationProvider) {

        $locationProvider.html5Mode(true);

    }])
  .run(function($rootScope, $state, Auth, $location, $window) {
    $rootScope
        .$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
               if (!Auth.authorize(toState.data.access)) {
                 event.preventDefault();

                 $state.go('anon.login');
               }
            });

      $rootScope
        .$on('$stateChangeSuccess',
            function(event){

                console.log($location.path());

                if (!$window.ga)
                    return;
                console.log('$window.ga');
                $window.ga('send', 'pageview');
                console.log("pageview hoyechhe from app page");

                if (!$window.fbq)
                    return;
                console.log('$window.fbq');
                $window.fbq('track', 'PageView');
            });


  })
    .config(function(toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-bottom-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });
    });
