angular.module('adminApp')
    .controller('BodyController', ['$scope','LocalService','CurrentUser','Auth','$state','$http', function ($scope,LocalService,CurrentUser,Auth,$state,$http) {

        $scope.logout = function() {
            console.log('calling body controller from admin');
            Auth.logout();
            $state.go('anon.login');
        };


        $scope.auth = Auth;

        $scope.currentUser = {};

        $scope.setLoggedinInfo = function(){
            $scope.currentUser = CurrentUser.user();
        };
        $scope.setLoggedinInfo();
        $scope.allfolders=[];
        $scope.rootfolderimg=[];
        $scope.selectedFolderimg=[];
        $scope.folderList=function () {
            $http.post('/settings/folderList').then(function(returnData){
                if(returnData.data.folders.length){
                    $scope.allfolders=returnData.data.folders;
                }
                if(returnData.data.images.length){
                    $scope.rootfolderimg=returnData.data.images;
                }

            },function(err){
                if(err)
                    console.log(err);
            });
        };



        $scope.rootimg_hide=true;
        $scope.currentFolderIndex=-1;
        $scope.currentFolder="";
        $scope.showImages=function (x,indx) {
            $scope.currentFolder=x;
            $scope.rootimg_hide=false;
            $scope.currentFolderIndex = indx;

            $http.post('/settings/upload',{returndata:x})
                .then(function(return_images){
                    $scope.selectedFolderimg=return_images.data.folderimages;
                },function(err){
                    if(err)
                        console.log(err);
                });
        };


        // $scope.setLogInfo=function () {
        //     $scope.userInfo=CurrentUser.user();
        //     console.log($scope.userInfo)
        // };
        //
        // $scope.setLogInfo();
        //
        // if($scope.userInfo.user_role=='editor'){
        //     $scope.editorinfo=false;
        //     console.log($scope.userInfo.user_role)
        // }else {
        //     $scope.editorinfo=true;
        // }

    }]);
