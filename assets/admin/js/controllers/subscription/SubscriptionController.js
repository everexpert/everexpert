angular.module('adminApp')
    .controller('SubscriptionController', ['$scope','$http','$stateParams','toastr', function ($scope, $http, $stateParams,toastr) {

        // $('select').select2({
        //   minimumResultsForSearch: Infinity
        // });

        $scope.subscriptions=[];

        $http({
            method: 'GET',
            url: '/subscription'
        }).then(function successCallback(response) {
            if(response.data.length){
                $scope.subscriptions=response.data;
                console.log($scope.subscriptions);
            }

        }, function errorCallback(response) {

        });
        // $http.get("subscriptions").then(function(subscribe){
        //     console.log(subscribe);
        //     if(products.data.length){
        //         $scope.subscribe=subscribe.data;
        //     }
        // },function(err){
        //     if(err)
        //         console.log(err);
        // });

        $scope.deleteEmail = function(subscription,index){
            $http.post('/subscription/destroy/'+subscription.id).then(function(deleteEmail){
                //console.log(deleteEmail);
                toastr.success('Success!', 'Deleted Successfully!!');
                $scope.subscriptions.splice(index,1);
            },function(err){
                if(err)
                    console.log(err);
                    toastr.success('error', 'Not Deleted');
            }).catch(function (data) {
                console.log(data);
            });
        }

    }]);

