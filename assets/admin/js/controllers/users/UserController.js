angular.module('adminApp')
    .controller('UserController', ['$scope','$http','$stateParams','toastr','Auth', function ($scope, $http, $stateParams,toastr,Auth) {

        // $('select').select2({
        //   minimumResultsForSearch: Infinity
        // });

        // $scope.auth = Auth;

        $scope.users=[];


        $http({
            method: 'GET',
            url: '/auth/findUser'
        }).then(function successCallback(response) {
            if(response.data.length){
                $scope.users=response.data;
                // console.log($scope.users);

                for(var i=0; i<$scope.users.length; i++){

                    if($scope.users[i].userRole == 'superAdmin' || $scope.users[i].userRole == 'admin'){
                        $scope.users[i].notAllow = true;
                        // console.log($scope.users[i])
                    }

                }


            }

        }, function errorCallback(response) {

        });
        $scope.deleteEmail = function(user,index){
            console.log(user)
            $http.post('/user/destroy/'+user.id).then(function(deleteEmail){
                //console.log(deleteEmail);
                toastr.success('Success!', 'Deleted Successfully!!');
                $scope.users.splice(index,1);
            },function(err){
                if(err)
                    console.log(err);
                    toastr.success('error', 'Not Deleted');
            }).catch(function (data) {
                console.log(data);
            });
        }


        $scope.save = function (user) {
            console.log("call hoise");

            $http.post('/auth/updateRole', user).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                console.log("success",response.data);


            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log("error",response)
            });

        }

        $scope.ShowUserForm = false;

        $scope.addNewUser = function () {
            $scope.ShowUserForm = true;
        }


    //    //////////////////////     //


        $scope.errors = [];
        $scope.user={};
        $scope.register = function() {
            $scope.errors = [];
            if(!$scope.user.hasOwnProperty("email") || $scope.user.email==''){
                $scope.errors = [];
                $scope.errors.push("Empty email");
                return;
            }

            if(!$scope.user.hasOwnProperty("password") || $scope.user.password==''){
                $scope.errors = [];
                $scope.errors.push("Empty password");
                return;
            }

            if(!$scope.user.hasOwnProperty("confirmPassword") || $scope.user.confirmPassword==''){
                $scope.errors = [];
                $scope.errors.push("Empty confirmPassword");
                return;
            }

            if($scope.user.password != $scope.user.confirmPassword){
                $scope.errors = [];
                $scope.errors.push("Password not matched");
                return;
            }

            $scope.errors = [];

            Auth.register($scope.user).then(function(user){
                console.log(user);

                $scope.users.push(user.data.user)
                $scope.ShowUserForm = false;
                toastr.success('Success', 'New user created');

                $scope.user={};

            },function(err) {
                console.log(err);
                toastr.error('Error', 'User not created');
                return;
            });



        }




    }]);

