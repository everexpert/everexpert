angular.module('adminApp')
    .controller('MediaManagerController', ['$scope','$http','$stateParams','toastr','Upload', function ($scope, $http, $stateParams,toastr,Upload) {

        $scope.folderList();

        $scope.onlyUploadImage = function (files) {
            console.log(files);

            $scope.files = files;
            for (var i = 0; i < $scope.files.length; i++) {
                var file = $scope.files[i];
                if(file.size<2000000){
                    Upload.upload({
                        url: '/settings/onlyUploadImage',
                        fields: {epectedDirectory:$scope.currentFolder},
                        file: file
                    }).success(function (data, status, headers, config) {
                        if($scope.currentFolder)
                            $scope.selectedFolderimg.push(data.customImage);
                        else
                            $scope.rootfolderimg.push(data.customImage);
                    });
                }else {
                    alert("please upload 1.8 mb file size file")
                }

            }

        };

        $scope.createSymlink = function(){
            $http.get('settings/createSymlink');
        };
        $scope.deleteThisImage = function(x,rootOrSubImageFolder){
            console.log($scope.rootfolderimg,$scope.selectedFolderimg);
            $http.post('/settings/onlyDeleteImage',{filePath:x}).then(function(response){
                console.log(response);
                if(response.data=='filePath deleted successfully'){
                    if(rootOrSubImageFolder == 'rootImageFolder'){
                        var index = $scope.rootfolderimg.indexOf(x);

                        if (index > -1) {
                            $scope.rootfolderimg.splice(index, 1);
                            toastr.success('Success!', 'Image deleted successfully');
                        }
                    }
                    else if(rootOrSubImageFolder == 'subImageFolder'){
                        var index = $scope.selectedFolderimg.indexOf(x);

                        if (index > -1) {
                            $scope.selectedFolderimg.splice(index, 1);
                            toastr.success('Success!', 'Image deleted successfully');
                        }
                    }
                }
            },function(err){
                if(err)
                    console.log(err);
                toastr.error('Failed to delete this image', 'Error!');
            })
        }

    }]);


