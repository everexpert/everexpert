angular.module('adminApp')
    .controller('RecoverController', function($scope, $state, Auth,$http) {

        $scope.user={};

        $scope.ss=false;

        $scope.foundEmail = '';

        // if($scope.foundEmail != '')

        $scope.send=function () {
            console.log($scope.user);

            $http.post('/auth/recover',$scope.user)
                .then(function(result) {
                    console.log("result",result);
                    if(result.data.findStatus=="Email found")
                        $scope.ss=true;

                    $scope.foundEmail ="Please check your email.";

                },function(err) {
                    console.log(err);
                });

        }

    });
