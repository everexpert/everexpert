angular.module('adminApp')
  .controller('LoginController', function($scope, $state, Auth) {
    $scope.errors = [];

    $scope.login = function() {
      $scope.errors = [];
      Auth.login($scope.user)
          .then(function(result) {
          console.log("result",result);
          $state.go('user.mediamanager');
      },function(err) {
              console.log(err);
        $scope.errors.push(err.data.err);
      });
    }



  });
