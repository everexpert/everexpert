angular.module('adminApp')
  .controller('EditSettingsController', ['$scope','$state','$http','$stateParams','Upload','toastr', function ($scope,$state,$http,$stateParams,Upload,toastr) {


    angular.element(document).ready(function (){
      $('select').select2({
        minimumResultsForSearch: Infinity
      });
    });


    $scope.addOperation='Update';
    $scope.settings={};

      $http.get('/settings').then(function(settings){
        if(settings.data.length){
          $scope.settings = settings.data[0];
        }else{
          $http.post('/settings',{}).then(function(settings){
            $scope.settings=settings.data;
            console.log($scope.settings);
          },function(err2){if(err2)console.log(err2)});
        }
      },function(err){if(err)console.log(err);});

      //$http({
      //  method: 'GET',
      //  url: '/settings/'+$stateParams.optionId
      //}).then(function successCallback(response) {
      //  console.log(response);
      //  if(response.data){
      //    $scope.option=response.data;
      //  }
      //}, function errorCallback(response) {
      //  console.log(response)
      //});


    $scope.saveSettings=function(){
      $http.post("/settings/update/"+$scope.settings.id,$scope.settings).then(function(settings){
          toastr.success('Success!', 'Settings updated successfully');
        // console.log(settings,"settings updated successfully");
      },function(err){
        if(err)
          console.log(err);
          toastr.error('Settings update Error', 'Error!');
      })
    }

  }]);
