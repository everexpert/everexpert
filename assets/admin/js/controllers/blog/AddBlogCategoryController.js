angular.module('adminApp')
    .controller('AddBlogCategoryController', ['$scope','$http','$stateParams','$state', 'Options', function ($scope, $http, $stateParams, $state, Options) {

        $scope.blogcategory={};
        $scope.addOperation='Add';

        $scope.save=function () {
            $http.post('/blogcategory',$scope.blogcategory).then(function(blogcategory){
                console.log(blogcategory);
                if(blogcategory){
                    $state.go("user.blog_category");
                }
            },function(err){
                if(err)
                    console.log(err);
            })
        };

        $scope.makeTitleSlug = function(){
            $scope.blogcategory.category_slug = Options.convertToSlug( $scope.blogcategory.category_title);
        };


    }]);

