angular.module('adminApp')
    .controller('AddBlogController', ['$scope','$http','$stateParams','$state','Options','Upload','toastr', function ($scope, $http, $stateParams,$state,Options,Upload,toastr) {

        angular.element(document).ready(function (){
            $('select').select2({
                minimumResultsForSearch: Infinity
            });
        });


        $scope.blog={};
        $scope.addOperation='Add';
        $scope.saveBlogs=function(){
            $http.post('/blog',$scope.blog).then(function(return_blog){
                // console.log(return_blog);
                if(return_blog){
                    $state.go("user.blog");
                }
            },function(err){
                if(err)
                    console.log(err);
            })
        };


        $scope.blogcategory=[];
        $http.get("blogcategory/").then(function(response){
            $scope.blogcategory=response.data;
            console.log($scope.blogcategory);
        },function(err){
            if(err)
                console.log(err);
        });


        $scope.makeTitleSlug = function(){
            $scope.blog.blogSlug = Options.convertToSlug( $scope.blog.blogTitle);
        };

        $scope.folderList();

        $scope.onlyUploadImage = function (files) {
            console.log(files);

            $scope.files = files;
            for (var i = 0; i < $scope.files.length; i++) {
                var file = $scope.files[i];
                if(file.size<2000000){
                    Upload.upload({
                        url: '/blog/onlyUploadImage',
                        fields: {epectedDirectory:$scope.currentFolder},
                        file: file
                    }).success(function (data, status, headers, config) {
                        if($scope.currentFolder)
                            $scope.selectedFolderimg.push(data.customImage);
                        else
                            $scope.rootfolderimg.push(data.customImage);
                    });
                }else {
                    alert("please upload 1.8 mb file size file")
                }

            }

        };

        $scope.createSymlink = function(){
            $http.get('blog/createSymlink');
        };
        $scope.deleteThisImage = function(x,rootOrSubImageFolder){
            console.log($scope.rootfolderimg,$scope.selectedFolderimg);
            $http.post('/blog/onlyDeleteImage',{filePath:x}).then(function(response){
                console.log(response);
                if(response.data=='filePath deleted successfully'){
                    if(rootOrSubImageFolder == 'rootImageFolder'){
                        var index = $scope.rootfolderimg.indexOf(x);

                        if (index > -1) {
                            $scope.rootfolderimg.splice(index, 1);
                            toastr.success('Success!', 'Image deleted successfully');
                        }
                    }
                    else if(rootOrSubImageFolder == 'subImageFolder'){
                        var index = $scope.selectedFolderimg.indexOf(x);

                        if (index > -1) {
                            $scope.selectedFolderimg.splice(index, 1);
                            toastr.success('Success!', 'Image deleted successfully');
                        }
                    }
                }
            },function(err){
                if(err)
                    console.log(err);
                toastr.error('Failed to delete this image', 'Error!');
            })
        }


        //image-select
        $scope.selectedImage=function (x) {
            $scope.blog.blogImage=x;
            console.log($scope.blog.blogImage);
            $('#myModal').modal('hide')
        }


    }]);

