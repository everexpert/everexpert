angular.module('adminApp')
    .controller('EditBlogCategoryController', ['$scope','$http','$stateParams','$state','Options',function ($scope, $http, $stateParams, $state,Options) {

        $scope.blogcategory={};
        $scope.addOperation='Edit';



        if($stateParams.hasOwnProperty('EditBlogCategoryId')){
            console.log($stateParams);
            $http.get('/blogcategory/'+$stateParams.EditBlogCategoryId).then(function(blogcategory){
                console.log(blogcategory);
                $scope.blogcategory=blogcategory.data;
            },function(err){
                if(err)
                    console.log(err);
            })
        }


        $scope.save=function () {
            $http.post('/blogcategory/update/'+$stateParams.EditBlogCategoryId,$scope.blogcategory).then(function(blogcategory){
                console.log(blogcategory);
                if(blogcategory){
                    $state.go("user.blog_category");
                }
            },function(err){
                if(err)
                    console.log(err);
            })
        };

        $scope.makeTitleSlug = function(){
            $scope.blogcategory.category_slug = Options.convertToSlug( $scope.blogcategory.category_title);
        };


    }]);

