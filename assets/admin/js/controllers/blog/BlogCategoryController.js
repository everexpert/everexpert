angular.module('adminApp')
    .controller('BlogCategoryController', ['$scope','$http','$stateParams', function ($scope, $http, $stateParams) {

        $scope.blogcategory=[];
        $scope.addOperation='Add';

        $http.get("blogcategory/").then(function(response){
            $scope.blogcategory=response.data;
            console.log($scope.blogcategory);
        },function(err){
            if(err)
                console.log(err);
        });


        $scope.deleteBlogCategory=function (x,index) {
            console.log(x,index);
            $http.post("blogcategory/destroy/"+x.id).then(function(){
                $scope.blogcategory.splice(index,1);
            },function(err){
                if(err)
                    console.log(err);
            });
        }



    }]);

