angular.module('adminApp')
.controller('BlogController', ['$scope','$http','$stateParams', function ($scope, $http, $stateParams) {

        $scope.blogs=[];
        $scope.addOperation='Add Blog';

        // $http.get("blog/findBlogs").then(function(blog){
        //     console.log(blog);
        //     if(blog.data.length){
        //         $scope.blogs=blog.data;
        //     }
        // },function(err){
        //     if(err)
        //         console.log(err);
        // });

        $scope.deleteBlog = function(x,index){
            $http.post('blog/destroy/'+x.id).then(function(return_blogs){
                console.log(return_blogs);
                $scope.blogs.splice(index,1);
            },function(err){
                if(err)
                    console.log(err);
            })
        }

    $scope.totalBlogs = 0;
    $scope.totalPages = [];
    $http.post('/blog/blogCount').then(function (response) {
        console.log('total blogs', response);
        if (response.data.hasOwnProperty('totalBlogs')) {
            $scope.totalBlogs = response.data.totalBlogs;
            for (i = 1; i <= Math.ceil($scope.totalBlogs /10); i++)
                $scope.totalPages.push(i);
        }


    }, function (err) {
        if (err)
            console.log(err);
    });

    $scope.currentPageNo = 0;

    $scope.setPage = function (pagNum) {
        $scope.currentPageNo = pagNum-1;
        $http.post("/blog/findBlogsByPage/", {pageNum: pagNum}).then(function (response) {
            if (response.data.length) {
                $scope.blogs = response.data;
                console.log($scope.blogs);
            }
        }, function (err) {
            if (err)
                console.log(err);
        });
    }

    if ($stateParams.hasOwnProperty('pageNum') && $stateParams.pageNum != 0)
        $scope.setPage($stateParams.pageNum);
    else
        $scope.setPage(1);



}]);

