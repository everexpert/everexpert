angular.module('adminApp')
  .factory('Options', ['$http', 'LocalService', 'AccessLevels', function($http, LocalService, AccessLevels) {
    return {
      getAllContinents: function() {
        //$http.get('/bower_components/countries/countries.min.json').
        //  success(function(data, status, headers, config) {
        //    console.log(data);
        //    console.log("ash");
        //    return data;
        //  })
        //return false;
      },
      getAllCountries: function() {
        //$http.get('/bower_components/countries/countries.min.json').
        //  success(function(data, status, headers, config) {
        //    return data.countries;
        //  })
        //return false;
      },
      getAllCountriesContinentWise: function() {
        //$http.get('/bower_components/countries/countries.min.json').
        //  success(function(data, status, headers, config) {
        //    var continents = data.continents;
        //    //console.log(data.continents);
        //    continents.forEach(function(obj) {
        //      console.log(obj);
        //    });
        //    return data.countries;
        //  })
        //return false;
      },
      convertToSlug: function (Text)
        {
          // return Text
          //   .toLowerCase()
          //   .replace(/[^\w ]+/g,'')
          //   .replace(/ +/g,'-')
          //   ;

          return  Text.toString().toLowerCase()
              .replace(/\s+/g, '-')           // Replace spaces with -
              .replace(/['"<>@+?.,\/#!$%\^&\*;:{}=\_`~()]/g,"")
              // .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
              .replace(/\-\-+/g, '-')         // Replace multiple - with single -
              .replace(/^-+/, '')             // Trim - from start of text
              .replace(/-+$/, '');            // Trim - from end of text
      }
    }
  }]);
