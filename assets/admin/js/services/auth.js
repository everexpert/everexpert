angular.module('adminApp')
  .factory('Auth', function($http, LocalService, AccessLevels,CurrentUser,$q) {
    return {
      authorize: function(access) {
        if (access === AccessLevels.user) {
          return this.isAuthenticated();
        } else {
          return true;
        }
      },
      isAuthenticated: function() {
        return LocalService.get('auth_teken');
      },
      login: function(credentials) {
        console.log('credentials',credentials);
        var login = $http.post('/auth/authenticate', credentials);


        return $q(function(resolve, reject) {
          login.then(function(result) {
            LocalService.set('auth_teken', JSON.stringify(result));
              resolve(result);
          },function(err){
            if(err)
              reject(err);
              console.log(err);
          });
        });
      },
      logout: function() {
        // The backend doesn't care about logouts, delete the token and you're good to go.
        LocalService.unset('auth_teken');
      },
      register: function(formData) {

        LocalService.unset('auth_teken');
        var deferred = $q.defer();
        var register = $http.post('/auth/register', formData);

        //register.then(function(result) {
        //  LocalService.set('auth_teken', JSON.stringify(result));
        //},function(err){
        //  if(err)
        //    return err;
        //});
        //return true;

        //register.then(function(result) {
        //  LocalService.set('auth_token', JSON.stringify(result));
        //  deferred.resolve(result);
        //},function(err){
        //  if(err)
        //    deferred.reject(msg);
        //});
        //return deferred.promise;


        return $q(function(resolve, reject) {
          register.then(function(result) {
            LocalService.set('auth_teken', JSON.stringify(result));
            resolve(result);
          },function(err){
            if(err)
              reject(err);
            console.log(err);
          });
        });


      }
    }
  })
  .factory('AuthInterceptor', function($q, $injector) {
    var LocalService = $injector.get('LocalService');

    return {
      request: function(config) {
        var token;
        if (LocalService.get('auth_teken')) {
          token = angular.fromJson(LocalService.get('auth_teken')).token;
        }
        if (token) {
          config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
      },
      responseError: function(response) {
        if (response.status === 401 || response.status === 403) {
          LocalService.unset('auth_teken');
          //$injector.get('$state').go('anon.login');
        }
        return $q.reject(response);
      }
    }
  })
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  });
