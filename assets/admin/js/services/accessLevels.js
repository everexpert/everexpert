angular.module('adminApp')
  .constant('AccessLevels', {
    anon: 0,
    user: 1
  });