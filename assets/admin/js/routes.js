angular.module('adminApp')
  .config(['$stateProvider', '$urlRouterProvider', 'AccessLevels', function($stateProvider, $urlRouterProvider, AccessLevels) {

    $stateProvider
        .state('anon', {
            abstract: true,
            template: '<ui-view/>',
            data: {
                access: AccessLevels.anon
            }
        })
        .state('anon.login', {
            url: '/login',
            templateUrl: 'admin/templates/auth/login.html',
            controller: 'LoginController'
        })
        .state('anon.recover', {
            url: '/recover',
            templateUrl: 'admin/templates/auth/recover.html',
            controller: 'RecoverController'
        })
        .state('anon.resetpassword', {
            url: '/resetpassword/:validationCode/:userEmail',
            templateUrl: 'admin/templates/auth/resetPassword.html',
            controller: 'ResetPasswordController'
        })
        .state('user', {
            abstract: true,
            templateUrl: 'admin/templates/base.html',
            data: {
                access: AccessLevels.user
            }
        })
       .state('user.home', {
            url: '/home',
            templateUrl: 'admin/templates/home/home.html',
            controller: 'HomeController'
        })
        .state('user.edit_settings', {
            url: '/edit-settings',
            templateUrl: 'admin/templates/settings/edit-settings.html',
            controller: 'EditSettingsController'
        })
        .state('user.order', {
            url: '/order/:pageNum',
            templateUrl: 'admin/templates/order/order.html',
            controller: 'OrderController'
        })
        .state('user.subscription', {
            url: '/subscription',
            templateUrl: 'admin/templates/subscription/subscription.html',
            controller: 'SubscriptionController'
        })
        .state('user.blog', {
            url: '/blog',
            templateUrl: 'admin/templates/blog/blog.html',
            controller: 'BlogController'
        })
        .state('user.add_blog', {
            url: '/add-blog',
            templateUrl: 'admin/templates/blog/add-edit-blog.html',
            controller: 'AddBlogController'
        })
        .state('user.edit_blog', {
            url: '/edit-blog/:blogId',
            templateUrl: 'admin/templates/blog/add-edit-blog.html',
            controller: 'EditBlogController'
        })
        .state('user.blogcomment', {
            url: '/blogcomment',
            templateUrl: 'admin/templates/blog/blogcomment.html',
            controller: 'BlogcommentsController'
        })

        .state('user.blog_category', {
            url: '/blog-category',
            templateUrl: 'admin/templates/blog/blog-category.html',
            controller: 'BlogCategoryController'
        })
        .state('user.add_blog_category', {
            url: '/add-blog-category',
            templateUrl: 'admin/templates/blog/add-edit-blog-category.html',
            controller: 'AddBlogCategoryController'
        })
        .state('user.edit_blog_category', {
            url: '/edit-blog-category/:EditBlogCategoryId',
            templateUrl: 'admin/templates/blog/add-edit-blog-category.html',
            controller: 'EditBlogCategoryController'
        })
        .state('user.mediamanager', {
            url: '/media-manager',
            templateUrl: 'admin/templates/media-manager/media.html',
            controller: 'MediaManagerController'
        })
        .state('user.user_record', {
            url: '/user',
            templateUrl: 'admin/templates/users/users.html',
            controller: 'UserController'
        });






    $urlRouterProvider.otherwise('/media-manager');

  }]);
